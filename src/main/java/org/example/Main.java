package org.example;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;

public class Main {
    public static void main(String[] args) {
        AnsiConsole.systemInstall();

        final Spiral s = new Spiral(6, 7);
        printSpiral(s);
    }

    protected static void printSpiral(Spiral spiral) {
        for (int y = 0; y < spiral.getDimY(); y++) {
            for (int x = 0; x < spiral.getDimX(); x++) {
                System.out.print(Ansi.ansi().fg(spiral.colors[y][x]).a("%4d".formatted(spiral.getValues()[y][x])).reset());
            }

            System.out.println();
        }
    }
}