package org.example;

import org.fusesource.jansi.Ansi.Color;

import java.util.Arrays;

public class Spiral extends Matrix {

    public Color[][] colors;

    public Spiral(int width, int height) {
        super(new int[height][width]);

        colors = new Color[height][width];

        for (int[] row : this.values) {
            Arrays.fill(row, -1);
        }

        for (Color[] row: colors) {
            Arrays.fill(row, Color.WHITE);
        }

        final Point pos = new Point(0, 0);
        final Point dir = new Point(1, 0);
        final Matrix rotate90 = new Matrix(new int[][] { { 0, -1 }, { 1, 0 }});

        boolean moved = false;
        pos.minus(dir);
        int counter = 1;

        for(;;) {
            pos.plus(dir);
            if (isEmpty(pos)) {
                values[pos.y][pos.x] = counter++;
                colors[pos.y][pos.x] = toColor(dir);
                moved = true;
            } else {
                if (!moved) {
                    break;
                }

                moved = false;
                pos.minus(dir);
                rotate90.multiply(dir);
            }
        }
    }

    public boolean isEmpty(Point pt) {
        if (pt.x >= 0 && pt.x < dimX) {
            if (pt.y >= 0 && pt.y < dimY) {
                return values[pt.y][pt.x] == -1;
            }
        }

        return false;
    }

    Color toColor(Point pt) {
        if (pt.x > 0) {
            return Color.CYAN;
        } else if (pt.x < 0) {
            return Color.GREEN;
        } else {
            if (pt.y > 0) {
                return Color.YELLOW;
            } else {
                return Color.RED;
            }
        }
    }
}
