package org.example;

public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void plus(Point b) {
        this.x += b.x;
        this.y += b.y;
    }

    public void minus(Point b) {
        this.x -= b.x;
        this.y -= b.y;
    }

}
