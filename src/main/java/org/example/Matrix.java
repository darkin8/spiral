package org.example;

import java.util.Objects;

public class Matrix {
    protected final int dimX;
    protected final int dimY;
    protected final int[][] values;

    public int[][] getValues() {
        return this.values;
    }

    public int getDimX() {
        return dimX;
    }

    public int getDimY() {
        return dimY;
    }

    public Matrix(int[][] values) {
        Objects.requireNonNull(values);
        int rows = values.length;

        if (rows > 0) {
            int cols = values[0].length;
            for (int[] row: values) {
                if (row.length != cols) {
                    throw new IllegalArgumentException("Wrong matrix configuration; line length expected: %d; actual: %d"
                            .formatted(cols, row.length));
                }
            }

            this.dimX = cols;
            this.dimY = rows;
            this.values = values;
        } else {
            this.dimX = 0;
            this.dimY = 0;
            this.values = values;
        }
    }

    public Matrix multiply(Matrix b) {
        if (this.dimX != b.dimY) {
            throw new IllegalArgumentException("Matrices should match by dimensions; incompatible: %d x %d <=> %d x %d"
                    .formatted(this.dimY, this.dimX, b.dimY, b.dimX));
        }

        final Matrix result = new Matrix(new int[dimY][b.dimX]);

        for (int r = 0; r < this.dimY; r++) {
            int[] row = this.values[r];

            for (int c = 0; c < b.dimX; c++) {
                for (int cm = 0; cm < row.length; cm++) {
                    result.values[r][c] += row[cm] * b.values[cm][c];
                }
            }
        }

        return result;
    }

    public void multiply(Point p) {
        final Matrix mtx = multiply(new Matrix(new int[][] { { p.x }, { p.y } }));
        p.x = mtx.values[0][0];
        p.y = mtx.values[1][0];
    }
}
